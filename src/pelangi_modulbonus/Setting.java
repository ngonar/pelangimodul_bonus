/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sca_modulbonus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ngonar
 */
public class Setting {

    private String dbUrl;
    private String dbDriver;
    private String dbUser;
    private String dbPass;
    private String cid;
    private String transactionFrom;
    private String ftpHost = "";
    private String ftpPort = "";
    private String ftpUsername = "";
    private String ftpPassword = "";
    private String mitra = "";
    private String partnerProduk = "";
    private String productCodeDesc = "";

    public Setting() {
        try {
            getFileSetting();
            //System.out.println("==>" + dbUrl + ", " + dbUser + ", " + dbPass);

        } catch (FileNotFoundException ex) {
            System.out.println("==>1");
            Logger.getLogger(Setting.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.out.println("==>2");
            Logger.getLogger(Setting.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    public void setConnection(String dbDriver) {
//        this.dbDriver = dbDriver;
//    }

    public Connection getConnection() {
        Connection con = null;
        System.out.println(dbUrl + ", " + dbUser + ", " + dbPass);
        try {
            Class.forName(dbDriver);
            con = DriverManager.getConnection(dbUrl, dbUser, dbPass);

            System.out.println("Connection OTO ok.");

            return con;

        } catch (Exception e) {
            System.err.println("Exception OTO : " + e.getMessage());
        }

        return con;
    }

    public String getCid() {
        return cid;
    }

    public String getdbURL() {
        return dbUrl;
    }
    
    private void getFileSetting() throws FileNotFoundException, IOException {
        File f = new File("setting.properties");
        if (f.exists()) {
            Properties pro = new Properties();
            FileInputStream in = new FileInputStream(f);
            pro.load(in);

            dbDriver = pro.getProperty("Application.database.driver");
            //setConnection(dbDriver);
            dbUrl = pro.getProperty("Application.database.url");

            dbUser = pro.getProperty("Application.database.user");
            dbPass = pro.getProperty("Application.database.pass");

            ftpHost = pro.getProperty("Application.ftp.host");
            ftpUsername = pro.getProperty("Application.ftp.username");
            ftpPassword = pro.getProperty("Application.ftp.password");

            cid = pro.getProperty("Switching.hulu.cid");

            mitra = pro.getProperty("Application.mitra");

            productCodeDesc = pro.getProperty("Application.produk.desc");
        } else {
            System.out.println("File setting not found");
        }
    }

    public String getTrxFrom() {
        return transactionFrom;
    }

    /**
     * @return the ftpHost
     */
    public String getFtpHost() {
        return ftpHost;
    }

    /**
     * @return the ftpPort
     */
    public String getFtpPort() {
        return ftpPort;
    }

    /**
     * @return the ftpUsername
     */
    public String getFtpUsername() {
        return ftpUsername;
    }

    /**
     * @return the ftpPassword
     */
    public String getFtpPassword() {
        return ftpPassword;
    }

    /**
     * @return the mitra
     */
    public String getMitra() {
        return mitra;
    }

    /**
     * @return the partnerProduk
     */
    public String getPartnerProduk()  throws FileNotFoundException, IOException {
        File f = new File("setting.properties");
        if (f.exists()) {
            Properties pro = new Properties();
            FileInputStream in = new FileInputStream(f);
            pro.load(in);

            partnerProduk = pro.getProperty("Application.produk");
        } else {
            System.out.println("File setting not found");
        }
        
        return partnerProduk;
    }

    /**
     * @param partnerProduk the partnerProduk to set
     */
    public void setPartnerProduk(String partnerProduk) {
        this.partnerProduk = partnerProduk;
    }

    /**
     * @return the productCodeDesc
     */
    public String getProductCodeDesc() {
        return productCodeDesc;
    }

    /**
     * @param productCodeDesc the productCodeDesc to set
     */
    public void setProductCodeDesc(String productCodeDesc) {
        this.productCodeDesc = productCodeDesc;
    }
}
