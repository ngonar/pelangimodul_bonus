/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pelangi_modulbonus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.*;

/**
 *
 * @author aldwin
 */
public class Main implements Runnable{
    
    public static String username,password,url,yesterday, today, bonusDate;
    public static Connection conn;
    public static Setting setting = new Setting();    
//    public static Statement statement;
    private static Logger logger = Logger.getLogger(Main.class);
    public Main(){
        String user_name,sql;
        try{
            Class.forName("org.postgresql.Driver").newInstance();
            //url="jdbc:postgresql://localhost/pelangi";
            url=setting.getdbURL();
            username ="postgres";
            password ="postgres";
            try{
                conn = DriverManager.getConnection(url,username,password);
                System.out.println("koneksi sukses");
            }catch(SQLException ex){
                System.out.println("gagal \n"+ ex);
            }
        }catch(Exception ex){
            System.out.println("Gak ketemu drivernya");
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
            String banyakHari = "";
        try {
            banyakHari = args[0];

        } catch (Exception e) {
            banyakHari = "5";
        }
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();

        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date tanggalBonus = new Date();

        Calendar cal = Calendar.getInstance();
        //cal.add(Calendar.DATE, -1);
        cal.add(Calendar.DATE, (Integer.parseInt(banyakHari) * (-1)));
        yesterday = dateFormat.format(cal.getTime())+" 00:00:00";
        today = dateFormat.format(date) +" 00:00:00";
        //bonusDate = dateFormat2.format(tanggalBonus);
        bonusDate = dateFormat.format(cal.getTime())+" 01:00:00";
        //System.out.println(yesterday); 
        //System.out.println(today);
        //System.out.println(dateFormat2.format(tanggalBonus));
        
        new Main();
        
        
         final Main main = new Main();
         try{
             conn = DriverManager.getConnection(url,username,password);
         
            Statement stcekbonus = conn.createStatement();            
            String sqlcekbon;
            sqlcekbon = "select id from bonuses where to_char(create_date,'YYYY/MM/DD')='"+dateFormat.format(date)+"';";
            System.out.println("select id from bonuses where to_char(create_date,'YYYY/MM/DD')='"+dateFormat.format(date)+"';");
            ResultSet rsbonus = stcekbonus.executeQuery(sqlcekbon);
         if (!rsbonus.isBeforeFirst()) {
             logger.log(Level.INFO, "-----------------STARTING------------------");
         new Thread(main).start();
         }else{
         System.out.println("Modul sudah pernah jalan hari ini");
         logger.log(Level.INFO, "Modul sudah pernah jalan hari ini");
         }
         }catch(Exception ex){
            System.out.println("Gak ketemu drivernya");
        }
         
    }
    
    public void run() {
        String user_name,sql;
        int  user_id,bonus;
        try{
            bonus = 0;
            Class.forName("org.postgresql.Driver").newInstance();
              try{
            conn = DriverManager.getConnection(url,username,password);
            Statement statement = conn.createStatement();
            Statement statementInsertBonMGM = conn.createStatement();
            sql = "select id,username from users where group_id=3 and price_template_id = 34;";
            ResultSet rs = statement.executeQuery(sql);
            while(rs.next()){
                user_id = Integer.parseInt(rs.getString("id"));
                user_name = rs.getString("username");
//                System.out.println(user_name + " - "+user_id);
                bonus =HitungBonus(user_id, 2);
//                System.out.println(user_name + " - "+user_id + " - "+bonus);
                if(bonus>0){
                String sqlbonusmgm = "INSERT INTO bonuses(" +
"            amount, inbox_id, user_id, create_date, status)" +
"    VALUES ("+bonus+", 0, "+user_id+", '"+bonusDate+"', 0);";
                statementInsertBonMGM.executeUpdate(sqlbonusmgm);
                logger.log(Level.INFO, user_name + " - "+user_id + " - "+bonus);
                logger.log(Level.INFO, sqlbonusmgm);}
                Thread.sleep(500);
            }             
            statement.close();
            statementInsertBonMGM.close();
            
            int bonus2 = 0;
            Statement statementLevel1 = conn.createStatement();
            Statement statementInsertBonLoket = conn.createStatement();
            String sqlLevel1 = "select id,username from users where group_id=3 and price_template_id = 2;";
            ResultSet rsLevel1 = statementLevel1.executeQuery(sqlLevel1);
            while(rsLevel1.next()){
                user_id = Integer.parseInt(rsLevel1.getString("id"));
                user_name = rsLevel1.getString("username");
//                System.out.println(user_name + " - "+user_id);
                bonus2 =HitungBonusLevel1(user_id) + HitungBonusLevel1to2(user_id);
                //System.out.println(user_name + " - "+user_id + " - "+bonus);
                if(bonus2>0){
                 String sqlbonusloket = "INSERT INTO bonuses(" +
"            amount, inbox_id, user_id, create_date, status)" +
"    VALUES ("+bonus2+", 0, "+user_id+", '"+bonusDate+"', 0);";
                statementInsertBonLoket.executeUpdate(sqlbonusloket);
                logger.log(Level.INFO, user_name + " - "+user_id + " - "+bonus2);
                logger.log(Level.INFO, sqlbonusloket);}
                Thread.sleep(500);
            }             
            statementLevel1.close();
            statementInsertBonLoket.close();
            
            int bonus3 = 0;
            Statement statementLevel2 = conn.createStatement();
            Statement statementInsertBonKTM = conn.createStatement();
            sql = "select id,username from users where group_id=3 and price_template_id = 64;";
            ResultSet rsLevel2 = statementLevel2.executeQuery(sql);
            while(rsLevel2.next()){
                user_id = Integer.parseInt(rsLevel2.getString("id"));
                user_name = rsLevel2.getString("username");
//                System.out.println(user_name + " - "+user_id);
                bonus3 =HitungBonusKTM(user_id, 2);
//                System.out.println(user_name + " - "+user_id + " - "+bonus);
                if(bonus3>0){
                String sqlbonusktm = "INSERT INTO bonuses(" +
"            amount, inbox_id, user_id, create_date, status)" +
"    VALUES ("+bonus3+", 0, "+user_id+", '"+bonusDate+"', 0);";
                statementInsertBonKTM.executeUpdate(sqlbonusktm);
                logger.log(Level.INFO, user_name + " - "+user_id + " - "+bonus3);
                logger.log(Level.INFO, sqlbonusktm);}
                Thread.sleep(500);
            }             
            statementLevel2.close();
            statementInsertBonKTM.close();            
            
            }catch(SQLException ex){
                System.out.println("gagal \n"+ ex);
            }
            }catch(Exception ex){
            System.out.println("Gak ketemu drivernya");
        }
        logger.log(Level.INFO, "FINISH MODUL BONUS");
    }
    
    public int NextDownline(int id){
        int bonus =0;
        String user_name,sql;
        int  user_id;
        try{
            
            
              try{
            Statement statementdown = conn.createStatement();  
            Statement statementInsertBonMGM = conn.createStatement();
            sql = "select id,username from users where group_id=3 and price_template_id = 34 and upline="+id+";";            
            ResultSet rsdown = statementdown.executeQuery(sql);                       
            while(rsdown.next()){
                user_id = Integer.parseInt(rsdown.getString("id"));
                user_name = rsdown.getString("username");
                
                NextDownline(user_id);
                bonus =HitungBonus(user_id, 2);
//                System.out.println("downline "+id+ " = " +user_name + " - "+user_id + " - "+bonus);
                Thread.sleep(500);
                if(bonus>0){
                String sqlbonusmgm = "INSERT INTO bonuses(" +
"            amount, inbox_id, user_id, create_date, status)" +
"    VALUES ("+bonus+", 0, "+user_id+", '"+bonusDate+"', 0);";
                statementInsertBonMGM.executeUpdate(sqlbonusmgm);
                logger.log(Level.INFO, user_name + " - "+user_id + " - "+bonus);
                logger.log(Level.INFO, sqlbonusmgm);
                }
            } 
            statementdown.close();
            statementInsertBonMGM.close();
            }catch(SQLException ex){
                System.out.println("gagal \n"+ ex);
            }
            }catch(Exception ex){
            System.out.println("Gak ketemu drivernya");
        }
        
        return 0;
        
    }
    
    public int HitungBonus(int id, int limit){
        int itungan =0;
        String user_name,sql;
        int  user_id,bonus;
        try{
            
              try{
            if(limit>0){
            Statement statementbon = conn.createStatement();            
//            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=3 and price_template_id = 34 and upline="+id+";";
            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=3 and price_template_id = 34 and upline="+id+" and transactions.create_date >='"+yesterday+"' and transactions.create_date <='"+today+"';";
//            System.out.println(sqlbonus);
            ResultSet rsbon = statementbon.executeQuery(sqlbonus);            
            if(limit==2){
            while(rsbon.next()){
            itungan = itungan + (Integer.parseInt(rsbon.getString("count"))*100);
            }    
            }
            else if(limit==1){
            while(rsbon.next()){
            itungan = itungan + (Integer.parseInt(rsbon.getString("count"))*50);
            }
            }
            Statement statementdown = conn.createStatement();            
            sql = "select id,username from users where group_id=3 and price_template_id = 34 and upline="+id+";";
            ResultSet rsdown = statementdown.executeQuery(sql);                       
            while(rsdown.next()){
                user_id = Integer.parseInt(rsdown.getString("id"));
//                user_name = rsdown.getString("username");
//                System.out.println("downline "+id+ " = " +user_name + " - "+user_id);
                itungan = itungan + HitungBonus(user_id, limit-1);
//                Thread.sleep(1000);
            } 
            statementdown.close();
            statementbon.close(); 
            }
            else{
                Statement statementbon = conn.createStatement();            
//            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=3 and price_template_id = 34 and upline="+id+";";
            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=3 and price_template_id = 34 and upline="+id+" and transactions.create_date >='"+yesterday+"' and transactions.create_date <='"+today+"';";
            
            ResultSet rsbon = statementbon.executeQuery(sqlbonus);
            while(rsbon.next()){
            itungan = itungan + (Integer.parseInt(rsbon.getString("count"))*50);
            }
            statementbon.close(); 
            }
            }catch(SQLException ex){
                System.out.println("gagal \n"+ ex);
            }
            }catch(Exception ex){
            System.out.println("Gak ketemu drivernya");
        }
        
        return itungan;
        
    }

    public int HitungBonusKTM(int id, int limit){
        int itungan =0;
        String user_name,sql;
        int  user_id,bonus;
        try{
            
              try{
            if(limit>0){
            Statement statementbon = conn.createStatement();            
//            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=3 and price_template_id = 34 and upline="+id+";";
            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=3 and price_template_id = 64 and upline="+id+" and transactions.create_date >='"+yesterday+"' and transactions.create_date <='"+today+"';";
//            System.out.println(sqlbonus);
            ResultSet rsbon = statementbon.executeQuery(sqlbonus);            
            if(limit==2){
            while(rsbon.next()){
            itungan = itungan + (Integer.parseInt(rsbon.getString("count"))*100);
            }    
            }
            else if(limit==1){
            while(rsbon.next()){
            itungan = itungan + (Integer.parseInt(rsbon.getString("count"))*50);
            }
            }
            Statement statementdown = conn.createStatement();            
            sql = "select id,username from users where group_id=3 and price_template_id = 64 and upline="+id+";";
            ResultSet rsdown = statementdown.executeQuery(sql);                       
            while(rsdown.next()){
                user_id = Integer.parseInt(rsdown.getString("id"));
//                user_name = rsdown.getString("username");
//                System.out.println("downline "+id+ " = " +user_name + " - "+user_id);
                itungan = itungan + HitungBonus(user_id, limit-1);
//                Thread.sleep(1000);
            } 
            statementdown.close();
            statementbon.close(); 
            }
            else{
                Statement statementbon = conn.createStatement();            
//            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=3 and price_template_id = 34 and upline="+id+";";
            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=3 and price_template_id = 64 and upline="+id+" and transactions.create_date >='"+yesterday+"' and transactions.create_date <='"+today+"';";
            
            ResultSet rsbon = statementbon.executeQuery(sqlbonus);
            while(rsbon.next()){
            itungan = itungan + (Integer.parseInt(rsbon.getString("count"))*50);
            }
            statementbon.close(); 
            }
            }catch(SQLException ex){
                System.out.println("gagal \n"+ ex);
            }
            }catch(Exception ex){
            System.out.println("Gak ketemu drivernya");
        }
        
        return itungan;
        
    }
    
        public int HitungBonusLevel1(int id){
        int itungan =0;
        String user_name,sql;
        int  user_id,bonus;
        try{
        Statement statementbonlev1 = conn.createStatement();
        Statement statementInsertBonLoket2 = conn.createStatement();
//            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=22 and price_template_id = 35 and upline="+id+";";
            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=22 and price_template_id = 35 and upline="+id+" and transactions.create_date >='"+yesterday+"' and transactions.create_date <='"+today+"';";
//            System.out.println(sqlbonus);
            ResultSet rsbon = statementbonlev1.executeQuery(sqlbonus);
            while(rsbon.next()){
            itungan = itungan + (Integer.parseInt(rsbon.getString("count"))*100);
//            Thread.sleep(1000);
            }
            
            statementbonlev1.close();
            Statement statementdownlev1 = conn.createStatement();            
            sql = "select id,username from users where group_id=22 and price_template_id = 35 and upline="+id+";";
            ResultSet rsdown = statementdownlev1.executeQuery(sql);                       
            while(rsdown.next()){
                user_id = Integer.parseInt(rsdown.getString("id"));
                user_name = rsdown.getString("username");
//                System.out.println("downline "+id+ " = " +user_name + " - "+user_id);
                int bonuslevel2 = HitungBonusLevel2(user_id);
                if(bonuslevel2>0){
                
                String sqlbonusloket2 = "INSERT INTO bonuses(" +
"            amount, inbox_id, user_id, create_date, status)" +
"    VALUES ("+bonuslevel2+", 0, "+user_id+", '"+bonusDate+"', 0);";
                statementInsertBonLoket2.executeUpdate(sqlbonusloket2);
                logger.log(Level.INFO, user_name + " - "+user_id + " - "+bonuslevel2);
                logger.log(Level.INFO, sqlbonusloket2);}
                itungan = itungan + bonuslevel2;                
//                Thread.sleep(1000);
            }
            statementInsertBonLoket2.close();
            statementdownlev1.close();
        }catch(Exception ex){
            System.out.println("Gak ketemu drivernya");
        }
        return itungan;
     }
        
        public int HitungBonusLevel2(int id){
        int itungan =0;
        String user_name,sql;
        int  user_id,bonus;
        try{
        Statement statementbonlev2 = conn.createStatement();            
//            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=23 and price_template_id = 36 and upline="+id+";";
            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=23 and price_template_id = 36 and upline="+id+" and transactions.create_date >='"+yesterday+"' and transactions.create_date <='"+today+"';";
//            System.out.println(sqlbonus);
            ResultSet rsbon = statementbonlev2.executeQuery(sqlbonus);
            while(rsbon.next()){
            itungan = itungan + (Integer.parseInt(rsbon.getString("count"))*100);
//            Thread.sleep(1000);
            }
            
            statementbonlev2.close();
            
        }catch(Exception ex){
            System.out.println("Gak ketemu drivernya");
        }
        return itungan;
     }
        
        public int HitungBonusLevel1to2(int id){
        int itungan =0;
        String user_name,sql;
        int  user_id,bonus;
        try{
        Statement statementbonlev2 = conn.createStatement();            
//            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=23 and price_template_id = 36 and upline="+id+";";
            String sqlbonus = "select count(transactions.id) from transactions inner join users on users.id=transactions.user_id where group_id=23 and price_template_id = 36 and upline="+id+" and transactions.create_date >='"+yesterday+"' and transactions.create_date <='"+today+"';";
//            System.out.println(sqlbonus);
            ResultSet rsbon = statementbonlev2.executeQuery(sqlbonus);
            while(rsbon.next()){
            itungan = itungan + (Integer.parseInt(rsbon.getString("count"))*200);
//            Thread.sleep(1000);
            }
            
            statementbonlev2.close();
            
        }catch(Exception ex){
            System.out.println("Gak ketemu drivernya");
        }
        return itungan;
     }
}